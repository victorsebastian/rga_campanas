﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using RGA.Business;
using RGA.Business.Helper;

namespace RGA.Campaign.SendMail
{
    class Program
    {
        static string _url,
            _user,
            _pass,
            _fromUserId,
            _deleteExcel,
            _emailSend;
        private static readonly ILog _log = LogManager.GetLogger("");
        private static CrmService _service;

        static void Main(string[] args)
        {
            var appSettings = ConfigurationManager.AppSettings;
            try
            {
                log4net.Config.XmlConfigurator.Configure();

                #region globalVariables
                _url = appSettings["Uri"];
                _user = appSettings["User"];
                _pass = appSettings["Pass"];
                _fromUserId = appSettings["fromMailIdUser"];
                _deleteExcel = appSettings["deleteTempExcel"];
                _emailSend = appSettings["emailSend"];
                #endregion

                #region conexion
                _log.Info("*** Iniciando conexión ***");
                //Generamos la conexión con D365
                var connectionString = "AuthType = AD; Username=" + _user + ";Password=" + _pass + ";URL=" + _url + ";";
                CrmServiceClient conn = new CrmServiceClient(connectionString);

                //Creamos el OrganizationService:
                _service = new CrmService(new Uri(_url), _user, _pass);
                #endregion


                _log.Info("***Connection Successful! ***");
                ProcesoEnvio procesoEnvioE = new ProcesoEnvio();
                // TO DO
                //1.- Buscar la campaña que tienes activa. 
                EntityCollection campaignEC = Campaigns.SearchActiveCampaign(_service.Service);

                //Comprobamos la última fecha de proceso de envío
                string lastDateExecute = ProcesoEnvio.SearchLastCreatedByAccount(_service.Service);

                foreach (Entity campaignE in campaignEC.Entities)
                {
                    try
                    {
                        procesoEnvioE.Clear();
                        procesoEnvioE.fechaInicio = DateTime.Now;
                        procesoEnvioE.campania = campaignE.Id.ToString();

                        //1.1.- Buscar la plantilla de correo asociada a la campaña
                        Entity plantillaCorreoE = EmailTemplate.FirstSearchbyName(_service.Service, campaignE.Attributes["rga_plantillaemailname"].ToString());
                        _log.Info("[EmailTemplate.FirstSearchbyName] Plantilla recuperada: " + plantillaCorreoE.Attributes["title"]);

                        //1.2.- Buscar las listas de marketing asociadas a la campaña
                        EntityCollection marketingListEC =
                            MarketingList.SearchByCampaign(_service.Service, campaignE.Id.ToString());
                        _log.Info("[MarketingList.SearchByCampaign] Listas de Marketing recuperadas. Número total: " + marketingListEC.Entities.Count);

                        //1.3.- Obtenemos todos los miembros de todas las listas de marketing
                        EntityCollection accountEC = Account.SearchByMarketingList(_service.Service, marketingListEC);
                        _log.Info("[Account.SearchByMarketingList] Cuentas recuperadas. Número total: " + accountEC.Entities.Count);

                        //1.4.- Para cada caja u oficina obtenemos los campaign member, creamos correo, creamos y adjuntamos el excel y envia correo
                        foreach (Entity accountE in accountEC.Entities)
                        {
                            try
                            {
                                //Control para recuperar el último proceso de envío relacionado con la cuenta
                                procesoEnvioE.cuenta = accountE.Id.ToString();

                                //1.4.1- Buscar los Campaign Member
                                EntityCollection campaignMemberEC = CampaignMemberByAccountType(accountE, lastDateExecute, campaignE.Attributes["campaignid"].ToString());
                                _log.Info(" Busqueda de campaign members para la cuenta: " + accountE.Attributes["name"].ToString() +
                                ". Se han encontrado un total de " + campaignMemberEC.Entities.Count + " Campaign members");
                                procesoEnvioE.nregistros = campaignMemberEC.Entities.Count;

                                if (campaignMemberEC.Entities.Count > 0)
                                {
                                    //1.4.2.- Crea Excel
                                    string excelPath = Excel.ExcelExport(campaignMemberEC,
                                        accountE.Attributes["name"].ToString(), accountE);
                                    _log.Info("Generación del fichero Excel: " + excelPath);

                                    //1.4.3- Montar correo 
                                    Guid emailId = CreateEmail(plantillaCorreoE, accountE, campaignE, excelPath);
                                    _log.Info("Email generado: " + emailId);

                                    //1.4.4- Marca registros como enviados
                                    campaignMemberEC = CampaignMember.setTrueAvisoCajaUOficina(campaignMemberEC, getTipoCuenta(accountE));
                                    var updateList = _service.UpdateRequest(campaignMemberEC.Entities.ToArray().ToList());
                                    var responseList = _service.ExecuteRequest(updateList, 300);
                                    ProcessResponses(responseList, ref procesoEnvioE, campaignMemberEC);
                                    _log.Info("Marcación de registros como envíados realizado. Número de registros actualizados: " + updateList.Count);

                                    //1.4.5.- Envio Correo
                                    if (_emailSend == "Y")
                                    {
                                        Email.SendEmail(_service.Service, emailId);
                                        _log.Info("[Email.SendEmail] Envío realizado");
                                    }
                                }

                                procesoEnvioE.fechaFin = DateTime.Now;
                                procesoEnvioE.CreateRegistreCRM(_service.Service);
                            }
                            catch (Exception ex)
                            {
                                procesoEnvioE.mensajeErrores += $"-- ERROR Cuenta {accountE["name"].ToString()}: {ex.Message} \n";
                                _log.Error($"ERROR Cuenta {accountE["name"].ToString()}: {ex.Message} --> {ex.StackTrace}");
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        procesoEnvioE.mensajeErrores += $"ERROR Campaña {campaignE["name"].ToString()}: {ex.Message}\n";
                        _log.Error($"ERROR Campaña {campaignE["name"].ToString()}: {ex.Message} --> {ex.StackTrace}");
                    }
                    finally
                    {
                        _log.Info("***Proceso finalizado ***");
                    }
                }

                
            }
            catch (Exception ex)
            {
                _log.Error($"At Main: {ex.Message}");
            }

        }

        private static void ProcesoEnvioControl()
        {

        }

        private static void ProcessResponses(List<ExecuteMultipleResponseItem> responses, ref ProcesoEnvio proc, EntityCollection updates)
        {
            for (int i = 0; i < responses.Count; i++)
            {
                var response = responses[i];

                if (response.Fault != null)
                {
                    _log.Warn($"Error en Actualizar Campaign Member: {updates[i]["rga_campaignmemberid"]} : {response.Fault.Message}");
                }
            }
        }


        public static string getTipoCuenta(Entity accountE)
        {
            string name = accountE.Contains("name") ? (string)accountE["name"] : string.Empty;

            if (!accountE.Contains("accountcategorycode"))
                throw new Exception($"at CampaignMemberByAccountType - La cuenta: {name} no posee tipificación.");

            var tipoCuenta = ((OptionSetValue)(accountE.Attributes["accountcategorycode"])).Value;
            string res = string.Empty;
            switch (tipoCuenta)
            {
                case 1:
                    //Tipo Caja
                    res = "caja";
                    break;
                case 2:
                    //Tipo Oficina
                    res = "oficina";
                    break;
            }

            return res;
        }

        public static EntityCollection CampaignMemberByAccountType(Entity accountE, string lastDate, string campaignId)
        {
            EntityCollection campaignMemberEC = null;
            string name = accountE.Contains("name") ? (string)accountE["name"] : string.Empty;

            var tipoCuenta = getTipoCuenta(accountE);

            try
            {
                switch (tipoCuenta)
                {
                    case "caja":
                        //Tipo Caja
                        campaignMemberEC = CampaignMember.SearchByOficinaOrCajaSinAviso(_service.Service, accountE["rga_codigo_caja"].ToString(), "caja", lastDate, campaignId);
                        break;
                    case "oficina":
                        //Tipo Oficina
                        campaignMemberEC = CampaignMember.SearchByOficinaOrCajaSinAviso(_service.Service, accountE["rga_codigo_oficina"].ToString(), "oficina", lastDate, campaignId);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"at CampaignMemberByAccountType - No se ha podido obtener los campaign memeber para: {name}");
            }
            return campaignMemberEC;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateE">Id de la plantilla de correo</param>
        /// <param name="addresseeE">Cuenta destinataria</param>
        /// <param name="regarding">Campaña referenciada en el correo</param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Guid CreateEmail(Entity templateE, Entity addresseeE, Entity regarding, string path)
        {
            if (!templateE.Contains("templateid"))
                throw new Exception($"at CreateEmail - No hay identificador de plantilla.");
            if (!addresseeE.Contains("accountid"))
                throw new Exception($"at CreateEmail - No hay identificador de cuenta.");
            if (!regarding.Contains("campaignid"))
                throw new Exception($"at CreateEmail - No hay identificador de campaña para la referenciación.");

            Guid fromUserGuid = Guid.Parse(_fromUserId);
            Guid emailId = Guid.Empty;

            //generamos correo con la plantilla
            if (templateE != null && addresseeE != null)
            {
                _log.Info("     ---Generación de correo---");
                Entity email = Email.CreateEmailWithTemplate(_service.Service, templateE, addresseeE);
                //rellenamos la información del correo

                emailId = Email.EmailActivityInformation(_service.Service, email, fromUserGuid, addresseeE, regarding.Id);
                //Adjuntamos fichero al correo
                if (path != null)
                {
                    _log.Info("---Añadir adjunto al correo---");
                    Email.AttachFileToMail(_service.Service, email, emailId, path);

                    //Eliminado de ficheros generados
                    if (_deleteExcel == "Y")
                    {
                        Excel.DeleteExcel(path);
                    }
                }
                else
                {
                    _log.Warn("---Ruta excel nula---");
                }
            }
            else
            {
                _log.Warn("     ---plantilla o cuentas sin información---");

            }

            return emailId;
        }
    }
}
