﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Security.Tokens;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;

namespace RGA.Business
{
    public class ProcesoEnvio
    {
        private string _logicalName = "rga_procesoenvio";
        //private string editor = "rc_" 

        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public int correosCreados { get; set; }
        public string mensajeErrores { get; set; }
        public string campania { get; set; }
        public string cuenta { get; set; }
        public int nregistros { get; set; }
        public void Clear()
        {
            fechaInicio = DateTime.MinValue;
            fechaFin = DateTime.MinValue;
            correosCreados = 0;
            mensajeErrores = string.Empty;
            campania = string.Empty;
            cuenta = string.Empty;
            nregistros = 0;
        }

        public Entity ConvertToEntity()
        {
            Entity ent = new Entity(_logicalName);
            ent["rga_fechainicio"] = fechaInicio;
            ent["rga_fechafin"] = fechaFin;
            ent["rga_mensajeerrores"] = mensajeErrores;
            ent["rga_registrosexcel"] = nregistros;
            if (campania != string.Empty)
                ent["rga_campaignid"] = new EntityReference("campaign", new Guid(campania));

            if (cuenta != string.Empty)
                ent["rga_accountid"] = new EntityReference("account", new Guid(cuenta));


            return ent;
        }

        public Guid CreateRegistreCRM(OrganizationServiceProxy service)
        {
            return service.Create(this.ConvertToEntity());
        }

        public Entity SearchLastCreatedByCampaign(OrganizationServiceProxy service, Guid id)
        {
            string str = @"";

            EntityCollection ec = service.RetrieveMultiple(new FetchExpression(str));

            if (ec.Entities.Count > 0)
            {
                return ec.Entities[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Método para buscar el último proceso de envío por la cuenta relacionada
        /// </summary>
        /// <param name="service"></param>
        /// <param name="id">Id de la cuenta relacionada</param>
        /// <returns></returns>
        public static string SearchLastCreatedByAccount(OrganizationServiceProxy service)
        {

            string date = string.Empty;
            string str = String.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false' top='1'>
                          <entity name='rga_procesoenvio'>
                            <attribute name='rga_procesoenvioid'/>
                            <attribute name='rga_name' />
                            <attribute name='createdon' />
                            <attribute name='rga_fechainicio' />
                            <attribute name='rga_fechafin' />
                            <attribute name='rga_accountid' />
                            <attribute name='rga_campaignid' />
                            <order attribute='createdon' descending='true' />
                          </entity>
                        </fetch>");
            EntityCollection procesoEC = service.RetrieveMultiple(new FetchExpression(str));
            if (procesoEC.Entities.Count > 0)
            {
                int result = 0;
                DateTime aux;
                foreach (Entity procesoE in procesoEC.Entities)
                {
                    if (procesoE.Attributes.Contains("rga_fechafin"))
                    {
                        date = procesoE.Attributes["rga_fechafin"].ToString();
                        aux = DateTime.Parse(date);
                        date = aux.ToString("yyyy'-'MM'-'dd");
                        result = DateTime.Compare(aux, DateTime.Now.AddDays(-1));

                        if (result < 0)
                        {
                            return date;
                        }
                        else
                        {
                            return DateTime.Now.ToString("yyyy'-'MM'-'dd");
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;

            }
            else
            {
                return null;
            }
        }

    }
}
