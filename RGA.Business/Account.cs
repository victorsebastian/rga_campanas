﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace RGA.Business
{
    public class Account
    {
        /// <summary>
        /// Búsqueda de las cuentas miembro de una lista de marketing concreta
        /// </summary>
        /// <param name="service">Servicio de CRM</param>
        /// <param name="id"><type>Guid de la marketing List</param>
        /// <returns></returns>
        public static EntityCollection SearchByMarketingList(IOrganizationService service, string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentException("id cannot be null or empty");

            string officeListFetch = String.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
                                                      <entity name='account'>
                                                        <attribute name='name' />
                                                        <attribute name='primarycontactid' />
                                                        <attribute name='accountid' />
	                                                    <attribute name='accountcategorycode' />
	                                                    <attribute name='rga_codigo_caja' />
	                                                    <attribute name='rga_codigo_oficina' />
                                                        <order attribute='name' descending='false' />
                                                        <link-entity name='listmember' from='entityid' to='accountid' visible='false' intersect='true'>
                                                          <link-entity name='list' from='listid' to='listid' alias='ac'>
                                                            <filter type='and'>
                                                              <condition attribute='listid' operator='eq' uitype='list' value='{0}' />
                                                            </filter>
                                                          </link-entity>
                                                        </link-entity>
                                                      </entity>
                                                    </fetch>", id);

            return service.RetrieveMultiple(new FetchExpression(officeListFetch));
        }

        /// <summary>
        /// Búsqueda de todas las cuentas pertenecientes a varias listas de marketing
        /// </summary>
        /// <param name="service"> Servicio de CRM</param>
        /// <param name="mlEC"> Listado de las Marketing List</param>
        /// <returns></returns>
        public static EntityCollection SearchByMarketingList(IOrganizationService service, EntityCollection mlEC)
        {
            if (mlEC == null)
                throw new ArgumentException("mlEC is null or empty");

            EntityCollection ec = new EntityCollection();
            foreach (var mlE in mlEC.Entities)
            {
                string id = mlE.Id.ToString();
                ec.Entities.AddRange(SearchByMarketingList(service, id).Entities);
            }
            return ec;
        }


        /// <summary>
        /// búsqueda de las cuentas secundarias de una cuenta primaria existente
        /// </summary>
        /// <param name="service">Servicio de CRM</param>
        /// <param name="id">Id de la cuenta</param>
        /// <returns></returns>
        public static EntityCollection SearchByPrimaryAccount(IOrganizationService service, string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentException("id is null or empty");

            //Fetch generada para recuperar las oficinas de una caja (cuenta primaria)
            string officeListFetch = String.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                      <entity name='account'>
                                                        <attribute name='name' />
                                                        <attribute name='primarycontactid' />
                                                        <attribute name='accountid' />
	                                                    <attribute name='accountcategorycode' />
	                                                    <attribute name='rga_codigo_caja' />
	                                                    <attribute name='rga_codigo_oficina' />
                                                        <order attribute='name' descending='false' />
                                                        <link-entity name='account' from='accountid' to='parentaccountid' alias='aa'>
                                                          <filter type='and'>
                                                            <condition attribute='accountid' operator='eq' uitype='account' value='{0}' />
                                                          </filter>
                                                        </link-entity>
                                                      </entity>
                                                    </fetch>", id);

            return service.RetrieveMultiple(new FetchExpression(officeListFetch));

        }

    }
}
