﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using RGA.Business.Helper;


namespace RGA.Business
{
    public class CampaignMember
    {
        /// <summary>
        /// Pasado un conjunto de campos y valores. Los convierte en entidad
        /// </summary>
        /// <param name="fieldList">Lista de campos. Propiedades de la entidad</param>
        /// <param name="valueList">Lista de valores a asignar</param>
        /// <returns></returns>
        public static Entity ConvertToEntity(List<string> fieldList, List<string> valueList)
        {
            if (fieldList == null)
                throw new ArgumentException("fieldList cannot be null");

            if (valueList == null)
                throw new ArgumentException("valueList cannot be null");

            Entity ent = new Entity("rga_campaignmember");
            for (int i = 0; i < fieldList.Count; i++)
            {
                ent[fieldList[i]] = valueList[i];
            }
            return ent;
        }

        /// <summary>
        /// Asigna a una entidad un id de campaña. 
        /// </summary>
        /// <param name="ent"></param>
        /// <param name="id">Id de Campaña</param>
        /// <returns></returns>
        public static Entity SetCampaign(Entity ent, Guid id)
        {
            if (id == Guid.Empty || id == null)
                throw new ArgumentException("id cannot be null or empty");

            if (ent == null)
                throw new ArgumentException("ent cannot be null or empty");

            ent["rga_campaignid"] = new EntityReference("campaign", id);
            return ent;
        }


        /// <summary>
        /// Método para buscar campaign members por una oficina o caja concreta
        /// </summary>
        /// <param name="service"></param>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="lastDate">Fecha de la última ejecución para el control. Si la fecha es null se usa la fecha de hoy</param>
        /// <returns></returns>
        public static EntityCollection SearchByOficinaOrCajaSinAviso(IOrganizationService service, string id, string type, string lastDate, string campaignId)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentException("id cannot be null or empty");

            var fieldAdvise = string.Empty;
            var field = string.Empty;

            switch (type)
            {
                case "oficina":
                    field = "rga_oficina";
                    fieldAdvise = "rga_aviso_oficina";
                    break;
                case "caja":
                    field = "rga_caja";
                    fieldAdvise = "rga_aviso_caja";
                    break;
                default:
                    throw new ArgumentException("type haven't match");
            };

            //Comprobamos la última fecha de ejecución de 'proceso envío'. Si la fecha está rellena (puede contener la fecha fin del último registro o la fecha de hoy.
            //En caso de que tenga esta fecha se añadirá como filtro a la fetch. en caso de que no tenga esta fecha no se filtrará la fetch para que recupere todos los registros
            string officeListFetch = string.Empty;
            if (lastDate == null)
            {
                officeListFetch = String.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                  <entity name='rga_campaignmember'>
                                                    <attribute name='rga_campaignmemberid' />
                                                    <attribute name='rga_name' />
                                                    <attribute name='rga_tipo_traspaso' />
                                                    <attribute name='rga_tipo_incumplimiento' />
                                                    <attribute name='rga_tipo_incentivo' />
                                                    <attribute name='rga_situacion_calendario' />
                                                    <attribute name='rga_plan_destino' />
                                                    <attribute name='rga_oficina' />
                                                    <attribute name='rga_nombre_completo' />
                                                    <attribute name='rga_nif' />
                                                    <attribute name='rga_incentivo' />
                                                    <attribute name='rga_importe_traspaso' />
                                                    <attribute name='rga_importe_bonificacion' />
                                                    <attribute name='rga_gestora_destino' />
                                                    <attribute name='rga_fecha_movimiento' />
                                                    <attribute name='rga_fecha_fin_permanencia' />
                                                    <attribute name='rga_contingencia' />
                                                    <attribute name='rga_campaignid' />
                                                    <attribute name='rga_campign' />
                                                    <attribute name='rga_caja' />
                                                    <attribute name='rga_aviso_oficina' />
                                                    <attribute name='rga_aviso_caja' />
                                                    <attribute name='rga_anio' />
                                                    <order attribute='rga_name' descending='false' />
                                                    <filter type='and'>
                                                      <condition attribute='{0}' operator='eq' value='0' />
                                                      <condition attribute='{1}' operator='eq' value='{2}' />
													  <condition attribute='rga_campaignid' operator='eq' value='{3}' />
                                                    </filter>
                                                  </entity>
                                                </fetch>", fieldAdvise, field, id, campaignId);
            }
            else
            {
                officeListFetch = String.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                  <entity name='rga_campaignmember'>
                                                    <attribute name='rga_campaignmemberid' />
                                                    <attribute name='rga_name' />
                                                    <attribute name='rga_tipo_traspaso' />
                                                    <attribute name='rga_tipo_incumplimiento' />
                                                    <attribute name='rga_tipo_incentivo' />
                                                    <attribute name='rga_situacion_calendario' />
                                                    <attribute name='rga_plan_destino' />
                                                    <attribute name='rga_oficina' />
                                                    <attribute name='rga_nombre_completo' />
                                                    <attribute name='rga_nif' />
                                                    <attribute name='rga_incentivo' />
                                                    <attribute name='rga_importe_traspaso' />
                                                    <attribute name='rga_importe_bonificacion' />
                                                    <attribute name='rga_gestora_destino' />
                                                    <attribute name='rga_fecha_movimiento' />
                                                    <attribute name='rga_fecha_fin_permanencia' />
                                                    <attribute name='rga_contingencia' />
                                                    <attribute name='rga_campaignid' />
                                                    <attribute name='rga_campign' />
                                                    <attribute name='rga_caja' />
                                                    <attribute name='rga_aviso_oficina' />
                                                    <attribute name='rga_aviso_caja' />
                                                    <attribute name='rga_anio' />
                                                    <order attribute='rga_name' descending='false' />
                                                    <filter type='and'>
                                                      <condition attribute='{0}' operator='eq' value='0' />
                                                      <condition attribute='{1}' operator='eq' value='{2}' />
                                                      <condition attribute='createdon' operator='on-or-after' value='{3}' />
													  <condition attribute='rga_campaignid' operator='eq' value='{4}' />
                                                    </filter>
                                                  </entity>
                                                </fetch>", fieldAdvise, field, id, lastDate, campaignId);
            }

            // Fecha de creación = HOY
            // o
            // Desde el último registro de rga_procesoenvio que tenga campo fecha_fin informado

            return service.RetrieveMultiple(new FetchExpression(officeListFetch));
        }

        /// <summary>
        /// Método para indicar en el campaign member mediante dos checks si ha sido incluido en algún aviso de caja y oficina. 
        /// Estos checks son independientes por lo que uno es exclusivo para el aviso de caja de igual forma que el otro lo es para el aviso a las oficinas.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="campaignMember"></param>
        /// <param name="checkControl"></param>
        public static EntityCollection setTrueAvisoCajaUOficina(EntityCollection campaignMember, string checkControl)
        {
            List<Entity> list = campaignMember.Entities.ToArray().ToList();

            switch (checkControl)
            {
                case "oficina":
                 list.ForEach(x => x["rga_aviso_oficina"] = true);
                 break;
                case "caja":
                 list.ForEach(x => x["rga_aviso_caja"] = true);
                 break;
            }
            campaignMember.Entities.Clear();
            campaignMember.Entities.AddRange(list);
            return campaignMember;
        }

        public static (List<Entity>, string, int) ContactAssignment(OrganizationServiceProxy service, List<Entity> entityLst)
        {
            int aux = 0;
            int auxModifed = 0;
            int errorCount = 0;
            string errorMessage = string.Empty;
            List<Entity> entityLstModified = new List<Entity>();

            foreach (Entity ent in entityLst)
            {
                if (!ent.Contains("rga_nif"))
                    throw new Exception("At RGA.Business.CampaignMember.ContactAssignment(): nif is empty or null");

                string fetchXML = String.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                              <entity name='contact'>
                                <attribute name='fullname' />
                                <attribute name='telephone1' />
                                <attribute name='emailaddress1' />
                                <attribute name='contactid' />
                                <order attribute='fullname' descending='false' />
                                <filter type='and'>
                                  <condition attribute='rga_numeroidentificativo' operator='eq' value='{0}' />
                                </filter>
                              </entity>
                            </fetch>", ent.Attributes["rga_nif"]);
                EntityCollection result = service.RetrieveMultiple(new FetchExpression(fetchXML));
                if (result.Entities.Count == 1)
                {
                    Guid contactId = Guid.Parse(result.Entities.First().Attributes["contactid"].ToString());
                    entityLstModified.AddRange(entityLst.GetRange(aux,1));
                    entityLstModified[auxModifed].Attributes.Add("rga_contact", new EntityReference("contact", contactId));
                    auxModifed++;
                }
                else if (result.Entities.Count < 1)
                {
                    errorCount++;
                    errorMessage += $"No existe contactos con el DNI: {ent["rga_nif"]};\n";
                }
                else
                {
                    errorCount++;
                    errorMessage += $"Existen varios contactos con el DNI: {ent["rga_nif"]};\n";
                }
                aux++;
            }
            return (entityLstModified, errorMessage, errorCount);
            

        }
    }

    
}
