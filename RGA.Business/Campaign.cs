﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;


namespace RGA.Business
{
    public static class Campaigns
    {
        /// <summary>
        /// Busca las campañas que estan activas
        /// </summary>
        /// <param name="service"></param>
        /// <returns>DEvuelve campañas en estado activo</returns>
        public static EntityCollection SearchActiveCampaign(IOrganizationService service)
        {
            //Fetch generada para la entidad Campaña estándar
            string campaignListFetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                          <entity name='campaign'>
                                            <attribute name='name' />
                                            <attribute name='istemplate' />
                                            <attribute name='statuscode' />
                                            <attribute name='createdon' />
                                            <attribute name='campaignid' />
                                            <attribute name='rga_plantillaemailname' />
                                            <order attribute='name' descending='true' />
                                            <filter type='and'>
                                              <condition attribute='statecode' operator='eq' value='0' />
	                                          <condition attribute='statuscode' operator='eq' value='1' />
                                            </filter>
                                          </entity>
                                        </fetch>";
            return  service.RetrieveMultiple(new FetchExpression(campaignListFetch));
        }
    }
}
