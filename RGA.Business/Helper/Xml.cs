﻿using System.Collections.Generic;
using System.Xml;


namespace RGA.Business.Helper
{
    public static class Xml
    {
        public static List<MappingObject> ReadXML(string filePath)
        {
            XmlDocument doc = new XmlDocument();
            List<MappingObject> resultList = new List<MappingObject>();

            doc.Load(filePath);

            XmlNodeList elemList = doc.GetElementsByTagName("field");
            foreach (XmlNode node in elemList)
            {
                resultList.Add( 
                    new MappingObject() { 
                        FileField = node.SelectNodes("file")[0].InnerText,
                        CrmField  = node.SelectNodes("crm")[0].InnerText
                        }
                );
            }
            return resultList;
        }
    }
}
