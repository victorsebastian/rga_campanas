﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;

namespace RGA.Business.Helper
{
    public class CrmService
    {

        private OrganizationServiceProxy _service;

        public OrganizationServiceProxy Service
        {
            get { return _service; }
        }



        public CrmService(Uri uri, string name, string pass)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ClientCredentials clientCredentials = new ClientCredentials();
            clientCredentials.UserName.UserName = name;
            clientCredentials.UserName.Password = pass;

            //Create your Organization Service Proxy  
            _service = new OrganizationServiceProxy(
                uri,
                null,
                clientCredentials,
                null);
        }

        public List<OrganizationRequest> CreateRequest(List<Entity> list)
        {
            List<OrganizationRequest> result = new List<OrganizationRequest>();
            foreach (Entity ent in list)
            {
                result.Add( new CreateRequest()
                {
                    Target = ent
                });
            }
            return result;
        }

        public OrganizationRequest CreateRequest(Entity ent)
        {
            return new CreateRequest()
            {
                Target = ent
            };
        }

        public List<OrganizationRequest> UpdateRequest(List<Entity> list)
        {
            List<OrganizationRequest> result = new List<OrganizationRequest>();
            foreach (Entity ent in list)
            {
                result.Add(new UpdateRequest()
                {
                    Target = ent
                });
            }
            return result;
        }

        public OrganizationRequest UpdateRequest(Entity ent)
        {
            return new UpdateRequest()
            {
                Target = ent
            };
        }

        public List<ExecuteMultipleResponseItem> ExecuteRequest(List<OrganizationRequest> listRequest, int quantityBatch)
        {

            ExecuteMultipleResponse emResponses;
            List<ExecuteMultipleResponseItem> result = new List<ExecuteMultipleResponseItem>();

            ExecuteMultipleRequest erm = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            int index = 0,
                count = quantityBatch;
            while (index < listRequest.Count)
            {
                if (index + quantityBatch > listRequest.Count)
                    erm.Requests.AddRange(listRequest.GetRange(index, listRequest.Count - index));
                else
                    erm.Requests.AddRange(listRequest.GetRange(index, count));


                emResponses = (ExecuteMultipleResponse)_service.Execute(erm);

                foreach (var response in emResponses.Responses)
                {
                    result.Add(response);
                }

                index += quantityBatch;
                //log.Info("Se envia peticion de: " + erm.Requests.Count + "Siguiente: " + index );
                erm.Requests.Clear();
            }

            return result;
        }
    }
}
