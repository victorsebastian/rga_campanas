﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGA.Business.Helper
{

    public class Email
    {
        /// <summary>
        /// Método para generar un correo desde una plantilla de correo electrónico existente
        /// </summary>
        /// <param name="service"></param>
        /// <param name="plantillaCorreo"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static Entity CreateEmailWithTemplate(IOrganizationService service, Entity plantillaCorreo, Entity account)
        {
            InstantiateTemplateRequest instTemplateReq = new InstantiateTemplateRequest
            {
                TemplateId = plantillaCorreo.Id,
                ObjectId = account.Id,
                ObjectType = account.LogicalName
            };
            InstantiateTemplateResponse response =(InstantiateTemplateResponse)service.Execute(instTemplateReq);
            return response.EntityCollection[0];

            
        }

        /// <summary>
        /// Método para rellenar la información referente al 'De', 'Para', 'Referente a' de un email previamente creado
        /// </summary>
        /// <param name="service"></param>
        /// <param name="email"></param>
        /// <param name="fromUserId"></param>
        /// <param name="toRecord"></param>
        /// <param name="campaignId"></param>
        /// <returns></returns>
        public static Guid EmailActivityInformation(IOrganizationService service, Entity email, Guid fromUserId, Entity toRecord, Guid campaignId)
        {

            //Creamos actividad de correo 
            Entity fromActivityParty = new Entity("activityparty");
            Entity toActivityParty = new Entity("activityparty");

            fromActivityParty["partyid"] = new EntityReference("systemuser", fromUserId);
            toActivityParty["partyid"] = new EntityReference(toRecord.LogicalName, toRecord.Id);
            
            email["from"] = new Entity[] { fromActivityParty };
            email["to"] = new Entity[] { toActivityParty };
            email["regardingobjectid"] = new EntityReference("campaign", campaignId);

            Guid emailId = service.Create(email);
            return emailId;
        }

        /// <summary>
        /// Método para añadir a un correo electrónico existente un fichero adjunto de tipo excel. 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="email"></param>
        /// <param name="emailId"></param>
        /// <param name="filePath"></param>
        public static void AttachFileToMail(IOrganizationService service, Entity email, Guid emailId, string filePath)
        {
            Entity attachment = new Entity("activitymimeattachment");

            attachment["subject"] = "Excel-CampaignMembers";
            //string fileName = email.Attributes["to"].ToString() + "_" + DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
            attachment["filename"] = "Excel-CampaignMembers.xlsx";
            byte[] fileStream = File.ReadAllBytes(filePath);
            attachment["body"] = Convert.ToBase64String(fileStream);
            attachment["mimetype"] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            attachment["attachmentnumber"] = 1;
            attachment["objectid"] = new EntityReference(email.LogicalName, emailId);
            attachment["objecttypecode"] = "email";

            service.Create(attachment);
        }

        /// <summary>
        /// Método para ejecutar la petición de envío de correo electrónico
        /// </summary>
        /// <param name="service"></param>
        /// <param name="emailId"></param>
        public static void SendEmail(IOrganizationService service, Guid emailId)
        {
            SendEmailRequest request = new SendEmailRequest
            {
                EmailId = emailId,
                IssueSend = true
            };
            service.Execute(request);
        }
    }
}
