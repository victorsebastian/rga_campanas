﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace RGA.Business.Helper
{
 

    public static class FileHelper
    {
        static string _testHeaderLine = ConfigurationManager.AppSettings["FileHeaderLine"];
        public static bool CheckHeader(string pathFile, string splitter, List<string> headerLst)
        {
            bool ok = true;
            int i = 0;

            string headerLine = File.ReadLines(pathFile).First().ToLower();
            var lineHeaderLst = ParseLine(headerLine, splitter);
            
            foreach (var item in headerLst)
            {
                ok = lineHeaderLst[i].ToLower() == item.ToLower();
                if (!ok) throw new Exception($"Función: CheckHeader, Message: Campo de cabecera de fichero '{lineHeaderLst[i]}' no coincide con '{item}' en fichero HeaderFile.xml");
                i++;
            }
            return ok;
        }


        public static List<string> ReadCsv(string path, string splitter)
        {
            List<string> lines = new List<string>();
            

            lines = File.ReadAllLines(path, Encoding.UTF8).ToList();

            if (_testHeaderLine == "Y")
            {
                lines.RemoveAt(0);
            }

            return lines;
        }

        public static List<string> ReadCsv(string path, string splitter, List<string> headerLst)
        {
            List<string> lines = new List<string>();
            bool headerOk = true;

            //>>+
            if(_testHeaderLine == "Y")
            {
                if (headerLst != null)
                    headerOk = FileHelper.CheckHeader(path, splitter, headerLst);
                else
                    throw new Exception("No puede ser nula la cabecera si se va a comprobar");
            }

            if (headerOk)
            {
                lines = ReadCsv(path, splitter);
            }
            return lines;
        }

        public static List<string> ReadAllCsv(string path,  string splitter, List<string> headerLst)
        {
            List<string> filePathLst = Directory.GetFiles(path, "*.csv").ToList();
            List<string> allLines = new List<string>();
            foreach (string pathStr in filePathLst)
            {
                allLines.AddRange(ReadCsv(pathStr, splitter, headerLst));
            }
            return allLines;
        }

        public static List<string> ParseLine(string line, string splitter)
        {
            return line.Split(splitter.ToCharArray()).ToList();
        }

        public static void MoveToProcesados(string filePath)
        {
            string file = Path.GetFileName(filePath);
            string filePath2 = filePath.Substring(0, filePath.IndexOf(file)) + "Procesados\\" + file;
            File.Move(filePath, filePath2);
        }
    }
}
