﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RGA.Business.Helper
{
    public class MappingObject
    {
        private string _fileField, _crmField;
        public string FileField
        {
            get { return _fileField; }
            set { _fileField = value; }
        }
        public string CrmField
        {
            get { return _crmField; }
            set { _crmField = value; }
        }
    }
}
