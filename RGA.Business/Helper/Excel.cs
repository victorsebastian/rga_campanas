﻿using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using Microsoft.Xrm.Sdk;
using System.IO;
using System.Configuration;


namespace RGA.Business.Helper
{
    public class Excel
    {
        public static void HeaderPrint()
        {
            var header = new List<Header>
            {

            };
        }

        /// <summary>
        /// generación y manipulación del fichero excel con una serie de campos indicados en el fichero HeadXML.
        /// se realiza la exportación de una colección de datos referente a los campaign member para su posterior envío en un correo.
        /// </summary>
        /// <param name="campaignMember"></param>
        /// <param name="destinatarioMail"></param>
        /// <returns></returns>
        public static string ExcelExport(EntityCollection campaignMember, string destinatarioMail, Entity accountE)
        {
            //Lectura fichero mapeo
            var appSettings = ConfigurationManager.AppSettings;
            var xmlFilePath = appSettings["HeaderFile"];
            var mappFile = Xml.ReadXML(xmlFilePath);
            
            //Excel configuration
            Application ExcelApp = new Application();
            Workbook ExcelWorkBook = null;
            Worksheet ExcelWorkSheet = null;

            ExcelApp.Visible = true;
            ExcelWorkBook = ExcelApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            ExcelWorkBook.WebOptions.Encoding = Microsoft.Office.Core.MsoEncoding.msoEncodingUTF8;
            ExcelWorkSheet = ExcelWorkBook.Worksheets[1];

            //Escritura excel
            int j = 1;
            bool aux = true;
            string pass = string.Empty;

            foreach (var a in mappFile)
            {
                int row = 2;
                ExcelWorkSheet.Cells[1, j] = a.FileField;
                foreach (Entity c in campaignMember.Entities)
                {
                    if (aux)
                    {
                        pass = passGenerator(c, accountE);
                        aux = false;
                    }
                    if (c.Contains(a.CrmField))
                    {
                        ExcelWorkSheet.Cells[row, j] = c.Attributes[a.CrmField].ToString();
                        row++;
                    }
                    else
                    {
                        row++;
                    }
                }
                j++;
            }

            ExcelWorkBook.Password = pass;
            //Gardado temporal del fichero en carpeta indicada
            var excelPath = appSettings["ExcelExportPath"];
            var fileName = excelPath + "CampaignExportFile-" + destinatarioMail.Replace(" ", "_") + "-"
                + DateTime.Now.AddDays(-1).ToString("yyyyMMdd") + ".xlsx";
            

            if (File.Exists(fileName))
            {
                DeleteExcel(fileName);
            }

            
            ExcelWorkBook.SaveAs(fileName);

            //Cerramos excel
            ExcelWorkBook.Close();
            ExcelApp.Quit();

            return fileName;
        }

        private static string passGenerator(Entity c, Entity accountE)
        {
            var tipoCuenta = ((OptionSetValue)(accountE.Attributes["accountcategorycode"])).Value;
            string pass = string.Empty;
            if (c.Contains("rga_caja"))
            {
                pass = c.Attributes["rga_caja"].ToString() + "_";
            }
            pass += DateTime.Now.Year;
            if (c.Contains("rga_oficina") && tipoCuenta != 1)
            {
                pass += "_" + c.Attributes["rga_oficina"].ToString();
            }
            return pass;
        }

        /// <summary>
        /// Método de eliminación del fichero (según la configuración del app.config)
        /// </summary>
        /// <param name="filePath"></param>
        public static void DeleteExcel(string filePath)
        {
            File.Delete(filePath);
        }

    }
    public class Header
    {
        public string name = "Name";
        public string telephone = "Telefono";

    }
}
