﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;


namespace RGA.Business
{
    public class MarketingList
    {
        /// <summary>
        /// búsqueda de las listas de marketing relacionadas con una campaña concreta indicada.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="idCampaign"></param>
        /// <returns></returns>
        public static EntityCollection SearchByCampaign(IOrganizationService service, string idCampaign)
        {
            if (string.IsNullOrEmpty(idCampaign))
                throw new Exception("At RGA.Business.MarketingList.SearchByCampaign idCampaign is empty or null");

            //Fetch generada para recuperar las listas de marketing asociadas a cada campaña
            string marketingListFetch = String.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
                                                      <entity name='list'>
                                                        <attribute name='listname' />
                                                        <attribute name='type' />
                                                        <attribute name='createdfromcode' />
                                                        <attribute name='lastusedon' />
                                                        <attribute name='purpose' />
                                                        <attribute name='listid' />
                                                        <order attribute='listname' descending='true' />
                                                        <link-entity name='campaignitem' from='entityid' to='listid' visible='false' intersect='true'>
                                                          <link-entity name='campaign' from='campaignid' to='campaignid' alias='ab'>
                                                            <filter type='and'>
                                                              <condition attribute='campaignid' operator='eq' uitype='campaign' value='{0}' />
                                                            </filter>
                                                          </link-entity>
                                                        </link-entity>
                                                      </entity>
                                                    </fetch>", idCampaign);

            return service.RetrieveMultiple(new FetchExpression(marketingListFetch));
            

        }
    }
}
