﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace RGA.Business
{
    public class EmailTemplate
    {
        /// <summary>
        /// Clase para la búsqueda de la plantilla de correo electrónico informada en la campaña correspondiente.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public static EntityCollection SearchByName(IOrganizationService service, string templateName)
        {
            //Fetch generada para las plantillas de correo 
            string emailTemplateListFetch = String.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                          <entity name='template'>
                                                            <attribute name='title' />
                                                            <attribute name='templatetypecode' />
                                                            <attribute name='ispersonal' />
                                                            <attribute name='languagecode' />
                                                            <attribute name='templateid' />
                                                            <order attribute='title' descending='false' />
                                                            <filter type='and'>
                                                              <condition attribute='title' operator='eq' value='{0}' />
                                                            </filter>
                                                          </entity>
                                                        </fetch>", templateName);

            EntityCollection emailTemplateResult = service.RetrieveMultiple(new FetchExpression(emailTemplateListFetch));
            if (emailTemplateResult.Entities.Count > 0) {
                return emailTemplateResult;
            }
            else
            {
                return null;
            }
            

        }



        /// <summary>
        /// Método  con el que solo se devolverá el primer registro encontrado.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public static Entity FirstSearchbyName(IOrganizationService service, string templateName)
        {
            if (string.IsNullOrEmpty(templateName))
                throw new Exception("At RGA.Business.EmailTemplate.FirstSearchbyName(): templateName is empty or null");
            
            EntityCollection plantillaCorreoEC = EmailTemplate.SearchByName(service, templateName);
            Entity plantillaCorreoE = new Entity();
            if (plantillaCorreoEC.Entities.Count > 0)
            {
                plantillaCorreoE = plantillaCorreoEC.Entities.First();
            }
            return plantillaCorreoE;
        }


        public static Entity SearchById(IOrganizationService service, string templateId)
        {
            if (string.IsNullOrEmpty(templateId))
                throw new Exception("At RGA.Business.EmailTemplate.FirstSearchbyId(): templateId is empty or null");

           return service.Retrieve("template",new Guid(templateId), new ColumnSet(true));
        }

    }
}
