﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace RGA.Business
{
    public class ProcesoCargaCampanas
    {
        private string _logicalName = "rga_importacionregistroscampania";
        //private string editor = "rc_" 

        public DateTime fechaInicio { get; set; }
        public string nombreFichero { get; set; }
        public int lineasTotales { get; set; }
        public int correctos { get; set; }
        public int errores { get; set; }
        public string mensajeErrores { get; set; }
        public string campania { get; set; }

        public void Clear()
        {
            fechaInicio = DateTime.MinValue;
            nombreFichero = string.Empty;
            lineasTotales = 0;
            correctos = 0;
            errores = 0;
            mensajeErrores = string.Empty;
            campania = string.Empty;
        }

        public Entity ConvertToEntity()
        {
            Entity ent = new Entity(_logicalName);
            ent["rga_fechainicio"] = fechaInicio;
            ent["rga_name"] = nombreFichero;
            ent["rga_lineastotales"] = lineasTotales;
            ent["rga_lineascorrectas"] = correctos;
            ent["rga_lineaserroneas"] = errores;
            ent["rga_mensajeerrores"] = mensajeErrores;

            if (campania != string.Empty)
                ent["rga_campaignid"] = new EntityReference("campaign", new Guid(campania));

            return ent;
        }

        public Guid CreateRegistreCRM(OrganizationServiceProxy service)
        {
            return service.Create(this.ConvertToEntity());
        }




    }
}
