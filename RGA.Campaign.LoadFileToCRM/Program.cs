﻿using log4net;
using RGA.Business.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RGA.Business;
using System.Configuration;
using Microsoft.Xrm.Sdk;

namespace RGA.Campaign.LoadFileToCRM
{

    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger("");
        private static string _extension, _path, _splitter, _campaignId;
        private static CrmService _service;
        private static ProcesoCargaCampanas _processTotal = new ProcesoCargaCampanas();


        /// <summary>
        /// Procesa un Excel o CSV y lo carga en CRM
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            try
            {
                _log.Info("*** Inicio Proceso Carga ***");
                _extension = ConfigurationManager.AppSettings["Extension"];
                _path = ConfigurationManager.AppSettings["PathToRead"];
                _splitter = ConfigurationManager.AppSettings["Splitter"];
                _campaignId = ConfigurationManager.AppSettings["CampaignId"];
                string uri = ConfigurationManager.AppSettings["Uri"],
                    name = ConfigurationManager.AppSettings["User"],
                    pass = ConfigurationManager.AppSettings["Pass"];
                _service = new CrmService(new Uri(uri), name, pass);

                MainProcess();

                _log.Info("*** Fin Proceso Carga ***");
            }
            catch (Exception ex)
            {
                _log.Error($"At Main: {ex.Message}");
            }
        }


        private static void MainProcess()
        {
            try
            {
                List<string> filePathLst = Directory.GetFiles(_path, "*." + _extension).ToList();

                foreach (string pathStr in filePathLst)
                {

                    _processTotal.Clear();
                    _processTotal.fechaInicio = DateTime.Now;
                    _processTotal.nombreFichero = Path.GetFileName(pathStr);
                    _processTotal.campania = _campaignId;

                    //1.- Lectura Fichero
                    List<Entity> entityLst = ReadExcelOrCsvFile(pathStr);
                    //>>+
                    var entityLstC = CampaignMember.ContactAssignment(_service.Service, entityLst);
                    AddLogMessage(entityLstC.Item2, entityLstC.Item3);
                    // FileHelper.MoveToProcesados(pathStr);
                    List<OrganizationRequest> requestLst = _service.CreateRequest(entityLstC.Item1);
                    //List<OrganizationRequest> requestLst = _service.CreateRequest(entityLst);
                    //<<-
                    List<ExecuteMultipleResponseItem> responses = _service.ExecuteRequest(requestLst, 300);
                    ProcessResponses(responses);


                    _processTotal.CreateRegistreCRM(_service.Service);
                }
            }
            catch (Exception ex)
            {
                _log.Error($"At MainProcess: {ex.Message}");

            }
        }

        private static void ProcessResponses(List<ExecuteMultipleResponseItem> responses)
        {
            foreach (var response in responses)
            {
                if (response.Fault != null)
                {
                    _log.Warn($"Error: {response.Fault.Message}");
                    _processTotal.errores++;
                }
                else _processTotal.correctos++;
            }
        }

        public static void AddLogMessage(string message, int cont)
        {
            if (message != string.Empty)
            {
                _log.Warn($"Error: {message}");
                _processTotal.mensajeErrores += message;
            }
            if (cont != 0)
                _processTotal.errores += cont;

        }

        private static List<Entity> ReadExcelOrCsvFile(string path)
        {

            List<Entity> result = new List<Entity>();

            string headerFile = ConfigurationManager.AppSettings["HeaderFile"];

            List<MappingObject> mappingLst = Xml.ReadXML("HeaderFile.xml");

            switch (_extension)
            {
                case "csv":
                    result = ReadCSV(path, mappingLst);
                    break;
                default:
                    _log.Info($"No está contemplada la extension: {_extension}");
                    break;
            }
            return result;
        }


        private static List<Entity> ReadCSV(string path, List<MappingObject> mappingLst)
        {
            List<Entity> result = new List<Entity>();
            int numLine = 1;

            List<string> headerLst = mappingLst.Select(x => x.FileField).ToList();
            List<string> crmFieldsLst = mappingLst.Select(x => x.CrmField).ToList();

            List<string> linesLst = FileHelper.ReadCsv(path, _splitter, headerLst);
            linesLst.RemoveAll(x => x == string.Empty);
            _processTotal.lineasTotales = linesLst.Count;

            foreach (string line in linesLst)
            {
                try
                {
                    List<string> lineSplitted = FileHelper.ParseLine(line, _splitter);
                    if (lineSplitted.Count == headerLst.Count)
                    {
                        Entity ent = CampaignMember.ConvertToEntity(crmFieldsLst, lineSplitted);
                        ent = CampaignMember.SetCampaign(ent, new Guid(_campaignId));
                        result.Add(ent);
                    }
                    else
                    {
                        string message =
                            $"Error en linea {numLine}: La linea contiene distinto numero de campos que la cabecera.";
                        _log.Warn(message);
                        _processTotal.errores++;
                        _processTotal.mensajeErrores += message + "\n";
                    }
                    numLine++;
                }
                catch (Exception e)
                {
                    _processTotal.errores++;
                    _log.Warn($"Error en linea {numLine}:" + e.Message);
                }
            }

            return result;
        }

    }

}
